
public class Fraction {
	private double num,denum;
	
	public Fraction(double numerateur,double denominateur){
		this.num=numerateur;
		this.denum=denominateur;
	}
	public Fraction(){
		this(0,0);
	}
	public String toString(){
		return "num"+Double.valueOf(num).toString()+" "+"denum:"+Double.valueOf(denum).toString();
	}
}
